package com.grishberg.resoursejsondeserializer.resources;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Класс для извлечения текстовых ресурсов
 * Created by grishberg on 26.04.16.
 */
public class ResourceReader {
    private static final String TAG = ResourceReader.class.getSimpleName();

    public static String getJsonFromResource(Context context, int resourceId) {
        InputStream inputStream = context.getResources().openRawResource(resourceId);
        return getStringFromResource(inputStream);
    }

    /**
     * Прочитать ресурс по имени файла
     *
     * @param context
     * @param resourceName
     * @return
     */
    public static String getJsonFromResource(final Context context, final String resourceName) {
        InputStream inputStream = context.getResources().openRawResource(
                context.getResources().getIdentifier(resourceName,
                        "raw", context.getPackageName()));
        return getStringFromResource(inputStream);
    }

    public static String getJsonFromResource(final Context context, final Resources resources, final String resourceName) {
        InputStream inputStream = resources.openRawResource(
                resources.getIdentifier(resourceName,
                        "raw", context.getPackageName()));
        return getStringFromResource(inputStream);
    }

    private static String getStringFromResource(final InputStream inputStream) {
        if (inputStream == null) {
            return null;
        }

        BufferedReader in = null;
        StringBuffer buf = new StringBuffer();

        try {
            in = new BufferedReader(new InputStreamReader(inputStream));
            String readLine;
            while ((readLine = in.readLine()) != null) {
                buf.append(readLine);
            }
            in.close();
        } catch (IOException e) {
            Log.e(TAG, "getJsonFromResource: ", e);
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return buf.toString();
    }
}

