package com.grishberg.resoursejsondeserializer.interceptors;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;

import com.grishberg.resoursejsondeserializer.resources.ResourceReader;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.Protocol;
import okhttp3.Response;
import okhttp3.ResponseBody;

/**
 * Created by grishberg on 02.10.16.
 */
public class TestSuccessStubInterceptor implements Interceptor {
    private static final String TAG = TestSuccessStubInterceptor.class.getSimpleName();
    private final Resources resources;
    private final Context context;

    public TestSuccessStubInterceptor(final Context context, final Resources resources) {
        this.resources = resources;
        this.context = context;
    }

    @Override
    public Response intercept(Interceptor.Chain chain) throws IOException {
        Response response;

        // Извлечь URI запроса.
        final HttpUrl uri = chain.request().url();
        Log.d(TAG, "intercept: " + uri);
        // извлечь запускаемый метод.
        final String path = uri.encodedPath();
        Log.d(TAG, "intercept: path = " + path);

        int code = 200;

        final String rawPath = preparePath(path);
        final String responseString = ResourceReader.getJsonFromResource(context, resources, rawPath);
        response = new Response.Builder()
                .code(code)
                .message(responseString)
                .request(chain.request())
                .protocol(Protocol.HTTP_1_0)
                .body(ResponseBody.create(MediaType.parse("application/json"), responseString.getBytes()))
                .addHeader("content-type", "application/json")
                .build();

        return response;
    }

    /**
     * Преобразовать URL в путь к файлу
     *
     * @param path
     * @return
     */
    private String preparePath(String path) {
        return path;
    }
}
